var express = require('express');
var compareJSON = require('json-structure-validator');

var router = express.Router();
var _ = require('lodash');
var metaDataStructure = {
	"clientId": "",
	"appId": "",
	"featureId": "",
	"subFeature": "",
	"metaData": ""
};

var metaDataStructureWithId = _.extend({"_id": ""}, metaDataStructure);

/**
 * To Validate the structure of the JSON file being sent from the client side.
 * @param  {[type]}   req  [description]
 * @param  {[type]}   res  [description]
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
var canProcessMetaDataPost = function (req, res, next) {
	var comparison = compareJSON(req.body, metaDataStructure);
	var result = "Valid Request";
	if (comparison != true) {
		comparison = compareJSON(req.body, metaDataStructureWithId);
	}
	if (comparison != true) {
		result = comparison;
		res.status(401).send(result);
	} else {
		next();
	}
}

/**
 * All the routing functions for the api goes here.
 *
 * @param  {[type]} app [description]
 * @return {[type]}     [description]
 */
/**
 module.exports = function(app) {
	var metaDataController = require("../routes/controller/metaDataController");
	var tenantDataController = require("../routes/controller/tenantDataController");
	var imageUploadController = require("../routes/controller/imageUploadController");

	//Metadata Creation route
	app.route("/api/createMetaData")
		.post(metaDataController.createMetaData);

	// Tenant Specific Routes
	app.route("/api/queryTenantMetaData/:clientId/:appId/:featureId")
		.get(tenantDataController.queryTenantMetaData);
	app.route("/api/queryTenantData/:clientId/:appId/:featureId")
		.post(tenantDataController.queryTenantData);
	app.route("/api/createTenantData/:clientId/:appId/:featureId")
		.post(tenantDataController.createTenantData);
	app.route("/api/queryTenantDataById/:clientId/:appId/:featureId")
		.post(tenantDataController.queryTenantDataById);
	app.route("/api/queryTenantDataUsingKeyword/:clientId/:appId/:featureId")
		.post(tenantDataController.queryTenantDataUsingKeyword);
	app.route("/api/deleteTenantData/:clientId/:appId/:featureId/:dataid")
		.delete(tenantDataController.deleteTenantData);


	// Image Upload Url
	app.route("/api/imageUpload")
		.post(imageUploadController.upload);

}
 */
var metaDataController = require("../routes/controller/metaDataController");
var tenantDataController = require("../routes/controller/tenantDataController");
var imageUploadController = require("../routes/controller/imageUploadController");
var paymentCustomerController = require("../routes/controller/paymentCustomerController");
var paymentChargeController = require("../routes/controller/paymentChargeController");
var jsbeautifierController = require("../routes/controller/jsbeautifierController");
var icaljsonController = require("../routes/controller/icaljsonController");
var gsController = require("../routes/controller/gsheetController");
var geodistanceController = require("../routes/controller/geoDistanceController");
var useridmController = require("../routes/controller/userIdmController");
var useridmNewController = require("../routes/controller/userIdmNewController");
var crawlerController = require("../routes/controller/crawlerController");
var utilsController = require("../routes/controller/utilsController");
var merchantController = require("../routes/controller/merchantController");
var couponController = require("../routes/controller/couponsController");
var paytmController = require("../routes/controller/paytmController");
var pgbuildController = require("../routes/controller/pgbuildController");
var peopleSoftController = require("../routes/controller/peopleSoftController");
var bannerController = require("../routes/controller/bannerController");
var webAdvisorController = require("../routes/controller/webAdvisorController");
var rssReaderController = require("../routes/controller/rssReaderController");
var freechargeController = require('./controller/freechargeController.js');
var oxigenController = require('./controller/oxigenController.js');
var utaustinController = require('./controller/utaustinController.js');

//Metadata Creation route
router.route("/api/createMetaData")
	.post(metaDataController.createMetaData);

// Tenant Specific Routes
router.route("/api/queryTenantMetaData/:clientId/:appId/:featureId")
	.get(tenantDataController.queryTenantMetaData);
router.route("/api/queryTenantData/:clientId/:appId/:featureId")
	.post(tenantDataController.queryTenantData);
router.route("/api/createTenantData/:clientId/:appId/:featureId")
	.post(tenantDataController.createTenantData);
router.route("/api/queryTenantDataById/:clientId/:appId/:featureId")
	.post(tenantDataController.queryTenantDataById);
router.route("/api/queryTenantDataUsingKeyword/:clientId/:appId/:featureId")
	.post(tenantDataController.queryTenantDataUsingKeyword);
router.route("/api/deleteTenantData/:clientId/:appId/:featureId/:dataid")
	.delete(tenantDataController.deleteTenantData);


// Image Upload Url
router.route("/api/imageUpload")
	.post(imageUploadController.upload);

//Stripe Urls

/******Customer Api's*********/
router.route("/payment/customer/create")
	.post(paymentCustomerController.create);

router.route("/payment/customer/getInfo/:id")
	.post(paymentCustomerController.getInfo);

router.route("/payment/customer/update/:id/:description")
	.post(paymentCustomerController.update);

router.route("/payment/customer/delete/:id/")
	.post(paymentCustomerController.delete);

/******Charge Api's*********/
router.route("/payment/charge/create")
	.post(paymentChargeController.create);

/**** JS beautifier API *******/
router.route("/beautify/js")
	.post(jsbeautifierController.beautify);

/**** ICAL to JSON API *******/
router.route("/ical2json/convert")
	.post(icaljsonController.convert);

/**** Google spreadsheet API *******/
router.route("/gsheet/finduser")
	.post(gsController.finduser);

router.route("/user/finduserbyroll")
	.post(gsController.finduserbyroll);
router.route("/user/finduserbyemail").post(gsController.finduserbyemail);

router.route("/user/sendotp").post(gsController.sendOTP);

router.route("/user/validateotp").post(gsController.validateOTP);
router.route("/user/sendemailotp").post(gsController.sendEmailOTP);

router.route("/user/validateemailotp").post(gsController.validateEmailOTP);


/** Geo distance API **/
router.route("/geo/distance")
	.post(geodistanceController.getdistance);

/** User Identity API */
router.route("/user/create").post(useridmController.createuser);
router.route("/user/update").post(useridmController.updateuser);

router.route("/user/authenticate").post(useridmController.authenticate);
router.route("/user/profile").post(useridmController.getuser);
router.route("/user/forgotpassword").post(useridmController.forgotpassword);
router.route("/user/resetpassword").post(useridmController.resetpassword);
router.route("/user/changepassword").post(useridmController.changepassword);

router.route("/nuser/validateEmail").post(useridmNewController.validateEmail);
router.route("/nuser/sendOTPToEmail").post(useridmNewController.sendOTPToEmail);
router.route("/nuser/validateEmailOTP").post(useridmNewController.validateEmailOTP);
router.route("/nuser/activateUser").post(useridmNewController.activateUser);
router.route("/nuser/auth").post(useridmNewController.auth);
router.get('/users/:tenant', useridmNewController.getAllUserDetails);
router.route("/nuser/updateuserprofile").post(useridmNewController.updateuserprofile);
router.route("/nuser/getuserprofile").post(useridmNewController.getuserprofile);
router.route("/nuser/forgotpassword").post(useridmNewController.forgotpassword);
router.route("/nuser/resetpassword").post(useridmNewController.resetpassword);
router.route("/nuser/changepassword").post(useridmNewController.changepassword);
router.route("/nuser/getProfile").post(useridmNewController.getProfile);

/** User management portal */
var authorization = function(req, res, next) {
	var authToken = 'c0c6a0ffc0df0c516d711fba96b713f7';
	var authData = req.headers.authorization.split(' ');
	if (authToken === authData[0] && authData[1] ) {
		req.tenantName = authData[1];
		return next();
	} else {
		return res.status(401).json({
			error: 'Not Authorized'
		});
	}
};

router.get('/nuser/getAllUsers', authorization, useridmNewController.getAllUsers);
router.post('/nuser/addMultipleUnregisteredUsers', authorization, useridmNewController.addMultipleUnregisteredUsers);
router.post('/nuser/deleteMultipleUnregisteredUsers', authorization, useridmNewController.deleteMultipleUnregisteredUsers);
router.put('/nuser/editUnregisteredUser', authorization, useridmNewController.editUnregisteredUser);
router.post('/nuser/user/validate', authorization, useridmNewController.validateUser);
router.post('/nuser/user/create', authorization, useridmNewController.createUser);
router.put('/nuser/validationProperties/update', authorization, useridmNewController.updateValidationProperties);
router.get('/nuser/validationProperties/get', authorization, useridmNewController.getValidationProperties);


/** Crawler API's **/
router.route("/crawler/crawl").post(crawlerController.crawl);
router.route("/crawler/status").post(crawlerController.crawlstatus);

/** UT AUstin Mulesoft API's **/
router.route("/ut/semesterinfo").post(utaustinController.semesterinfo);
router.route("/ut/courseinfo").post(utaustinController.courseinfo);


/** Utilities **/
router.route("/utils/image/encode").post(utilsController.encode);

router.post('/utils/proxy', utilsController.proxyCall);

/******** Coupons Api's *******/
router.route("/coupons/create")
	.post(couponController.create);

router.route("/coupons/getCoupons")
	.post(couponController.getCoupons);

/******merchant api's*******/
router.route("/merchant/create")
	.post(merchantController.create);

router.route("/merchant/updateMerchant")
	.post(merchantController.updateMerchantUS);

router.route("/merchant/getMerchants")
	.post(merchantController.getMerchants);

router.route("/v1/bigdeal/us/getMerchants")
	.post(merchantController.getMerchantsUS);

router.route("/merchant/addUserOrder")
	.post(merchantController.addUserOrder);

router.route("/v1/bigdeal/us/addUserOrder")
	.post(merchantController.addUserOrderUS);

router.route("/merchant/getUserOrders")
	.post(merchantController.getUserOrders);

router.route("/v1/bigdeal/us/getUserOrders")
	.post(merchantController.getUserOrdersUS);

router.route("/merchant/getTenantOrders")
	.post(merchantController.getTenantOrders);

router.route("/merchant/getMerchantOrders")
	.post(merchantController.getMerchantOrders);

router.route("/v1/bigdeal/us/getMerchantOrders")
	.post(merchantController.getMerchantOrdersUS);

router.route("/v1/bigdeal/us/merchantLogin")
	.post(merchantController.merchantLoginUS);

router.route("/v1/bigdeal/us/getOrderStatus")
	.post(merchantController.getBigDealOrderStatusUS);

router.route("/v1/bigdeal/us/getOrders")
	.post(merchantController.getBigDealOrdersUS);

router.route("/v1/bigdeal/us/getOrdersDetail")
	.post(merchantController.getBigDealOrdersDetailUS);

router.route("/v1/bigdeal/us/merchantOrderConfirm")
	.post(merchantController.merchantOrderResponseUS);

router.route("/v1/bigdeal/us/merchantOrderStatusUpdate")
	.post(merchantController.merchantOrderStatusUpdateUS);

router.route("/v1/bigdeal/us/calc/delivery")
	.post(merchantController.calcDelivery);

router.route("/v1/bigdeal/us/merchants")
	.get(merchantController.getAllMerchantsUS);

router.route("/v1/bigdeal/us/token")
	.post(merchantController.updateMerchantToken);

router.route("/v1/bigdeal/us/autodecline")
	.get(merchantController.autoDeclineOrder);


/*********** Merchant api's for BIGDEAL (PRODUCTION)**********/

router.route("/v1/bigdeal/addUserOrder")
	.post(merchantController.addBigDealUserOrder);

router.route("/v1/bigdeal/getUserOrders")
	.post(merchantController.getBigDealUserOrders);

router.route("/v1/bigdeal/getMerchantOrders")
	.post(merchantController.getBigDealMerchantOrders);

router.route("/v1/bigdeal/createMerchants")
	.post(merchantController.createBigDealMerchant);

router.route("/v1/bigdeal/updateMerchants")
	.post(merchantController.updateBigDealMerchant);

router.route("/v1/bigdeal/merchantLogin")
	.post(merchantController.merchantLogin);

router.route("/v1/bigdeal/getMerchants")
	.post(merchantController.getBigDealMerchants);

router.route("/v1/bigdeal/getCanteenMerchants")
	.post(merchantController.getBigDealCanteenMerchants);

router.route("/bigdeal/merchantResponse")
	.get(merchantController.merchantResponse);

router.route("/v1/bigdeal/getOrderStatus")
	.post(merchantController.getBigDealOrderStatus);

router.route("/v1/bigdeal/getOrders")
	.post(merchantController.getBigDealOrders);

router.route("/v1/bigdeal/getOrdersDetail")
	.post(merchantController.getBigDealOrdersDetail);

router.route("/v1/bigdeal/merchantOrderConfirm")
	.post(merchantController.merchantOrderResponse);

router.route("/v1/bigdeal/merchantOrderStatusUpdate")
	.post(merchantController.merchantOrderStatusUpdate);

router.post('/v1/bigdeal/merchant', merchantController.getBigdealMerchant);


/*********** Merchant api's for BIGDEAL (QA)**********/

router.route("/v1/bigdeal/addUserOrderTest")
	.post(merchantController.addBigDealUserOrderTest);

router.route("/v1/bigdeal/getUserOrdersTest")
	.post(merchantController.getBigDealUserOrdersTest);

router.route("/v1/bigdeal/getMerchantOrdersTest")
	.post(merchantController.getBigDealMerchantOrdersTest);

router.route("/v1/bigdeal/createMerchantsTest")
	.post(merchantController.createBigDealMerchantTest);

router.route("/v1/bigdeal/updateMerchantsTest")
	.post(merchantController.updateBigDealMerchantTest);

router.route("/v1/bigdeal/getCanteenMerchantsTest")
	.post(merchantController.getBigDealCanteenMerchantsTest);

router.route("/v1/bigdeal/merchantLoginTest")
	.post(merchantController.merchantLoginTest);

router.route("/v1/bigdeal/getMerchantsTest")
	.post(merchantController.getBigDealMerchantsTest);

router.route("/v1/bigdeal/getOrderStatusTest")
	.post(merchantController.getBigDealOrderStatusTest);

router.route("/v1/bigdeal/getOrdersTest")
	.post(merchantController.getBigDealOrdersTest);

router.route("/v1/bigdeal/getOrdersDetailTest")
	.post(merchantController.getBigDealOrdersDetailTest);

router.route("/v1/bigdeal/merchantOrderConfirmTest")
	.post(merchantController.merchantOrderResponseTest);

router.route("/v1/bigdeal/merchantOrderStatusUpdateTest")
	.post(merchantController.merchantOrderStatusUpdateTest);


/*********Analytics for BigDeal Production************/

router.route("/v1/bigdeal/updatePageCountAnalytics")
	.post(merchantController.updatePageCountAnalytics);

router.route("/v1/bigdeal/getPageCountAnalytics")
	.post(merchantController.getPageCountAnalytics);

router.route("/v1/bigdeal/updateRestCountAnalytics")
	.post(merchantController.updateRestCountAnalytics);

router.route("/v1/bigdeal/getRestCountAnalytics")
	.post(merchantController.getRestCountAnalytics);

router.route("/v1/bigdeal/updateGetCodeCountAnalytics")
	.post(merchantController.updateCouponCountAnalytics);

router.route("/v1/bigdeal/getGetCodeCountAnalytics")
	.post(merchantController.getCouponCountAnalytics);

router.route("/v1/bigdeal/addBigDealAvailCoupon")
	.post(merchantController.addBigDealAvailCoupon);

router.route("/v1/bigdeal/getBigDealAvailCoupon")
	.post(merchantController.getBigDealAvailCoupon);

router.route("/v1/bigdeal/saveBookStoreDetail")
	.post(merchantController.saveBookStoreDetail);

router.route("/v1/bigdeal/getBookDetail")
	.post(merchantController.getBookDetail);

/*********Analytics for BigDeal QA***********/

router.route("/v1/bigdeal/updatePageCountAnalyticsTest")
	.post(merchantController.updatePageCountAnalyticsTest);

router.route("/v1/bigdeal/getPageCountAnalyticsTest")
	.post(merchantController.getPageCountAnalyticsTest);

router.route("/v1/bigdeal/updateRestCountAnalyticsTest")
	.post(merchantController.updateRestCountAnalyticsTest);

router.route("/v1/bigdeal/getRestCountAnalyticsTest")
	.post(merchantController.getRestCountAnalyticsTest);

router.route("/v1/bigdeal/updateGetCodeCountAnalyticsTest")
	.post(merchantController.updateCouponCountAnalyticsTest);

router.route("/v1/bigdeal/getGetCodeCountAnalyticsTest")
	.post(merchantController.getCouponCountAnalyticsTest);

router.route("/v1/bigdeal/addBigDealAvailCouponTest")
	.post(merchantController.addBigDealAvailCouponTest);

router.route("/v1/bigdeal/getBigDealAvailCouponTest")
	.post(merchantController.getBigDealAvailCouponTest);


/*********PeopleSoft API***********/

router.route("/ps/:id")
	.post(peopleSoftController.peopleSoftServices);
router.route("/ps/:id")
	.get(peopleSoftController.peopleSoftServices);


/*********Banner API***********/
router.route("/services/student/:id")
	.post(bannerController.bannerServices);
router.route("/services/student/:id")
	.get(bannerController.bannerServices);

router.route("/services/authenticate/login")
	.post(bannerController.authenticate);


/*********WebAdvisor API***********/

router.route("/COLLEAGUE/:id")
	.post(webAdvisorController.webAdvisorServices);
router.route("/COLLEAGUE/:id")
	.get(webAdvisorController.webAdvisorServices);

/*router.route("/merchant/adddeals")
 .post(merchantController.adddeals);

 router.route("/merchant/getdeals")
 .post(merchantController.getdeals);*/

/* Rss feed reader */
router.route("/rss/rssreader")
	.post(rssReaderController.rssReader);


/**** PAY API's *****/
router.route("/paytm/generatechecksum").post(paytmController.generatechecksum);
router.route("/paytm/verifychecksum").post(paytmController.verifychecksum);


/** PG Build API **/

router.route("/pgb/pgblogin").post(pgbuildController.pgblogin);


/**   FreeCharge Api **/

router.post('/freecharge/generatechecksum', freechargeController.generatechecksum);
router.post('/freecharge/generateMerchantChecksum', freechargeController.generateMerchantChecksum);
router.post('/freecharge/saveaddmoney', freechargeController.saveAddMoneyParam);
router.get('/freecharge/getaddmoney/:metadata', freechargeController.getAddMoneyResponse);
router.post('/freecharge/merchantAuth', freechargeController.merchantAuth);
router.post('/freecharge/addMerchantTransactions', freechargeController.addMerchantTransactions);
router.post('/freecharge/getMerchantTransactions', freechargeController.getMerchantTransactions);
router.post('/freecharge/updateMerchantTransactions', freechargeController.updateMerchantTransactions);


/**
 * Oxigen Wallet Api
 * */

router.post('/oxigen/loadmoney', oxigenController.saveloadmoneytransaction);
router.get('/oxigen/loadmoney/:mtrxid', oxigenController.getloadmoneytransaction);
router.get('/oxigen/opKeys',oxigenController.getOperatorKeywords); 
router.get('/oxigen/regionAlias',oxigenController.getRegionAliasMapping); 
module.exports = router;