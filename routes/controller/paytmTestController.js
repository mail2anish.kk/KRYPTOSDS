"use strict";
var paytm_config = require('../../paytm/paytm_config').paytm_Testconfig;
var paytm_checksum = require('../../paytm/checksum');
var querystring = require('querystring');
function htmlEscape(str) {
  return String(str)
          .replace(/&/g, '&amp;')
          .replace(/"/g, '&quot;')
          .replace(/'/g, '&#39;')
          .replace(/</g, '&lt;')
          .replace(/>/g, '&gt;');
}

exports.generatechecksum = function(request, response, next) {
    if(request.method == 'POST'){
        var fullBody = request.body;
        console.log("full body generatechecksum : " + JSON.stringify(fullBody));
        var decodedBody = fullBody;//querystring.parse(fullBody);
        try {
            console.log("gencecksum Decoded body : " + JSON.stringify(decodedBody));
        }catch(ex) {
            console.log("Error while printing decodedbody in generatechecksum " + ex);
        }
        paytm_checksum.genchecksum(decodedBody, paytm_config.MERCHANT_KEY, function (err, res) {
            response.writeHead(200, {'Content-type' : 'text/json','Cache-Control': 'no-cache'});
            response.write(JSON.stringify(res));
            response.end();
        });
    }else{
        response.writeHead(200, {'Content-type' : 'text/json'});
        response.end();
    }
}

exports.verifychecksum = function(request, response, next) {
    if(request.method == 'POST'){
        var fullBody = request.body;
        //request.on('end', function() {
            console.log("full body verifychecksum :  " + JSON.stringify(fullBody));
            var decodedBody = fullBody; //querystring.parse(fullBody);
            try {
                console.log("verifychecksum Decoded body : " + JSON.stringify(decodedBody));
            }catch(ex) {
                console.log("Error while printing decodedbody in verifychecksum " + ex);
            }
            //response.writeHead(200, {'Content-type' : 'text/html','Cache-Control': 'no-cache'});
            if(paytm_checksum.verifychecksum(decodedBody, paytm_config.MERCHANT_KEY)) {
                decodedBody.IS_CHECKSUM_VALID="Y";
            }else{
                decodedBody.IS_CHECKSUM_VALID="N";
            }
            if(decodedBody.CHECKSUMHASH){
                delete decodedBody.CHECKSUMHASH;
            }
            var data = '<html><head><meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-I">';
            data += '<title>Paytm</title>';
            data += '<script type="text/javascript">';
            data += 'function response(){';
            data += 'return document.getElementById("response").value;';
            data += '}';
            data += '</script>';
            data += '</head>';
            data += '<body>';
            data += 'Redirect back to the app<br>';
            data += '<form name="frm" method="post">';
            data += '<input type="hidden" id="response" name="responseField" value=\'' + htmlEscape(JSON.stringify(decodedBody)) + '\'>';
            data += '</form>';
            data += '</body>';
            data += '</html>';
            response.send(data);
            /*response.write('<html>');
            response.write('<head>');
            response.write('<meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-I">');
            response.write('<title>Paytm</title>');
            response.write('<script type="text/javascript">');
            response.write('function response(){');
            response.write('return document.getElementById("response").value;');
            response.write('}');
            response.write('</script>');
            response.write('</head>');
            response.write('<body>');
            response.write('Redirect back to the app<br>');
            response.write('<form name="frm" method="post">');
            response.write('<input type="hidden" id="response" name="responseField" value=\'' + htmlEscape(JSON.stringify(decodedBody)) + '\'>');
            response.write('</form>');
            response.write('</body>');
            response.write('</html>');                  
            console.log("called 2")
            response.end();*/
        //});
    }else{
        response.writeHead(200, {'Content-type' : 'text/json'});
        response.end();
    }
}