'use-strict'

/*-Module import*/
crypto = require('crypto');
var ObjectId = require('mongodb').ObjectID;
/*Global Vars*/
const merchant_key = "3b06e723-d2d5-4a54-91d5-761d1c6d6a18";
var dbUtil = require("../../config/dbUtil");


exports.generatechecksum = function (req, res) {
    var algorithm = 'SHA256';
    var json_request = sortParams(req.body);
    var plainText = JSON.stringify(json_request) + merchant_key;
    var checksum = crypto.createHash('sha256').update(plainText).digest('hex');
    res.send(checksum);
};

exports.generateMerchantChecksum = function (req, res) {
    var algorithm = 'SHA256';
    var plainText = req.body.merchantID+""+req.body.terminalID+""+req.body.transactionRefNumber+""+'abc';
    
    var checksum = crypto.createHash('sha256').update(plainText).digest('hex');
    res.send(checksum);
};

function sortParams(params) {
    var tempKeys = Object.keys(params);
    tempKeys.sort();
    var requestData = {};
    tempKeys.forEach(function (key) {
        if (key !== 'checksum') {
            if (params[key] === 'null') params[key] = '';
            requestData[key] = params[key];
        }
    });
    return requestData;
}

exports.saveAddMoneyParam = function (req, res) {

    var walletBalance = req.body.walletBalance;
    var errorCode = req.body.errorCode;
    var errorMessage = req.body.errorMessage;
    var status = req.body.status;
    var checksum = req.body.checksum;
    var metadata = req.body.metadata;

    try {
        dbUtil.getConnection(function (db) {
            var tableName = "T_ADD_MONEY_RESPONSE";
            var data = {
                walletBalance: walletBalance,
                errorCode: errorCode,
                errorMessage: errorMessage,
                status: status,
                checksum: checksum,
                metadata: metadata
            };
            db.collection(tableName).insertOne(data, function (err, result) {
                if (err)
                    res.json({success: false});
                else
                    res.json({success: true});
            });
        });

    } catch (e) {

    }

}

exports.getAddMoneyResponse = function (req, res) {

    var metadata = req.params.metadata;

    try {
        dbUtil.getConnection(function (db) {
            var tableName = "T_ADD_MONEY_RESPONSE";
            db.collection(tableName).find(
                {
                    metadata: metadata
                })
                .toArray(function (err, result) {
                    if (err)
                        res.json({success: false});
                    else
                        res.json({success: true, data: result[0]})
                });
        });

    } catch (e) {

        console.log(e);

    }

}


exports.merchantAuth = function (req, res) {

    var uname = req.body.username;
    var upass = req.body.password;

    try {
        dbUtil.getConnection(function (db) {
            var tableName = "T_FCMERCHANTS_LIST";
            db.collection(tableName).find(req.body).toArray(function (err, result) {
                    if (err)
                        res.json({success: false});
                    else
                        res.json({success: true, data: result[0]})
                });
        });

    } catch (e) {

        console.log(e);

    }

}

exports.addMerchantTransactions = function (req, res) {

    var email = req.body.email;
    var amt = req.body.amt;
    var txnId = req.body.txnId;
    var status = req.body.status;
    var addInfo = req.body.addInfo;
    var phnum=req.body.phnum;
    var date = new Date();
    date = date.toString();

    try {
        dbUtil.getConnection(function (db) {
            var tableName = "T_FCMERCHANT_TRANSACTIONS";
            db.collection(tableName).find({"email":email}).toArray(function (err, result) {
                //console.log('Result = '+ result);
                    if(result.length > 0){
                        result[0].transactionDetails.push({
                            "amt":amt,
                            "txnId":txnId,
                            "date":date,
                            "status":status,
                            "addInfo":addInfo,
                            phnum:phnum
                        });
                        
                        console.log('Result = '+ result[0].transactionDetails);
                        var id = new ObjectId(result[0].id);
                        var updatedData = {
                            "email":result[0].email,
                            "transactionDetails" : result[0].transactionDetails,
                        }
                        console.log(updatedData);

                        db.collection(tableName).updateOne({"email": email}, {$set: {"transactionDetails":result[0].transactionDetails}}, function (err, result3) {
                            res.json({
                                "success": "transactionDetails added"
                            });
                        });
                    }else{
                        var txn=[];
                        txn.push({
                            "amt":amt,
                            "txnId":txnId,
                            "date":date,
                            "status":status,
                            "addInfo":addInfo
                        })
                        var data = {
                            "email":email,
                            "transactionDetails":txn
                        }
                        db.collection(tableName).insertOne(data, function (err, result) {
                            res.json({
                                "success": "transactionDetails added"
                            });
                        });
                    }
                });
        });

    } catch (e) {

        console.log(e);

    }

}

exports.getMerchantTransactions = function (req, res) {

    var email = req.body.email;
    try {
        dbUtil.getConnection(function (db) {
            var tableName = "T_FCMERCHANT_TRANSACTIONS";
            db.collection(tableName).find({"email":email}).sort({
                _id: -1
            }).toArray(function (err, result) {
                    if(result.length > 0){
                        res.json({
                                "success": "transactionDetails",
                                "result": result
                        });
                    }else{
                            res.json({
                                "error": "no transactionDetails"
                        });
                    }
                });
        });
    } catch (e) {
        console.log(e);
    }

}

exports.updateMerchantTransactions = function (req, res) {

    var email = req.body.email;
    var txnId = req.body.txnId;
    try {
        dbUtil.getConnection(function (db) {
            var tableName = "T_FCMERCHANT_TRANSACTIONS";
            db.collection(tableName).find({"email":email}).toArray(function (err, result) {
                //console.log('Result = '+ result);
                    if(result.length > 0){
                        for(var i=0;i<result[0].transactionDetails.length;i++){
                            if(result[0].transactionDetails[i].txnId == txnId){
                               result[0].transactionDetails[i].status = "refund";
                            }
                        }

                        db.collection(tableName).updateOne({"email": email}, {$set: {"transactionDetails":result[0].transactionDetails}}, function (err, result3) {
                            res.json({
                                "success": "transactionDetails updated"
                            });
                        });
                    }else{
                            res.json({
                                "error": "no transactionDetails"
                        });
                    }
                });
        });
    } catch (e) {
        console.log(e);
    }
}