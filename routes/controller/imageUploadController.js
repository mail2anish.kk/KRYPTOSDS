var fs = require("fs");
var path = require("path");
var uuid = require("node-uuid");

/**
 * Uploads a base 64 encoded image to the filesystem and returns the url.
 *
 * @param  {[type]}   req  [description]
 * @param  {[type]}   res  [description]
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
exports.upload = function(req, res, next) {
	//var imageUrlAccessBasePath = req.protocol+"://"+req.hostname+":"+(process.env.PORT||"3000")+"/kryptosds/imagegallery/";
var imageUrlAccessBasePath = "https://kryptosda.kryptosmobile.com/kryptosds/imagegallery/";
	var imageUrlAccessPath = imageUrlAccessBasePath+"failed";
	var imageWritePath = path.normalize(__dirname+"../../../public/imagegallery/");
	console.log(" Normlized path "+ imageWritePath+uuid.v4());
	if (req.body && req.body.imageData && req.body.imageExtn) {
		var base64Data = req.body.imageData.replace(/^data:image\/png;base64,/, "").replace(/^data:image\/jpg;base64,/, "").replace(/^data:image\/jpeg;base64,/, "");
		var fileName = uuid.v4()+"."+req.body.imageExtn;
		fs.writeFileSync(imageWritePath+fileName, base64Data, 'base64', function(err){
			res.status(401).json({"Error":"Could not write due to "+err.message});
		});
		imageUrlAccessPath = imageUrlAccessBasePath+fileName;
	} else {
		res.status(401).json({"Error":"Body should contain 'imageData' and 'imageExtn'"});
	}
	res.json({"url":imageUrlAccessPath});
}
