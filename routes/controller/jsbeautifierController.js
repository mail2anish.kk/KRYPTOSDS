var beautify = require('js-beautify').js_beautify;
var beautify_html = require('js-beautify').html;
var beautify_css = require('js-beautify').css;

/**
 * Uploads a base 64 encoded image to the filesystem and returns the url.
 *
 * @param  {[type]}   req  [description]
 * @param  {[type]}   res  [description]
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
exports.beautify = function(req, res, next) {
	var result = "";
	if (req.body && req.body.code && req.body.codetype) {
		if(req.body.codetype === 'js') {
			result = beautify(req.body.code, { indent_size: 2 });	
		}else if(req.body.codetype === 'html') {
			result = beautify_html(req.body.code, { indent_size: 2 });		
		}else if(req.body.codetype === 'css') {
			result = beautify_css(req.body.code, { indent_size: 2 });		
		}		
	} else {
		res.status(401).json({"Error":"Body should contain 'code' and 'codetype'"});
	}
	res.json({"result":result});
}
