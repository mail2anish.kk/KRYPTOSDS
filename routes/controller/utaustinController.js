var request = require('request');

exports.semesterinfo = function(req, res, next) {
    if(req.body.endpoint == null || req.body.endpoint == undefined || req.body.endpoint.length == 0){
		res.jsonp({"error":"missing mandatory parameter."});
		return;
	}
	var endpoint = req.body.endpoint; 
	var options = {
	  url: endpoint,
	  headers: {
	    'client_id': '7406a20d400f4a5bbb4505fb8327c3c6',
	    'client_secret' : '5f42f721ff4e43c2A2CE59F3BB2EEDD3'
	  }
	};
 
	function callback(error, response, body) {
	  if (!error && response.statusCode == 200) {
	    var info = JSON.parse(body);
	    res.json(info);
	  }else {
	  	res.status(response.statusCode).json(body);
	  }
	}
	request(options, callback); 	
}

exports.courseinfo = function(req, res, next) {
    if(req.body.endpoint == null || req.body.endpoint == undefined || req.body.endpoint.length == 0){
		res.jsonp({"error":"missing mandatory parameter."});
		return;
	}
	var endpoint = req.body.endpoint; 
	var options = {
	  url: endpoint,
	  headers: {
	    'client_id': '7406a20d400f4a5bbb4505fb8327c3c6',
	    'client_secret' : '5f42f721ff4e43c2A2CE59F3BB2EEDD3'
	  }
	};
 
	function callback(error, response, body) {
	  if (!error && response.statusCode == 200) {
	    var info = JSON.parse(body);
	    res.json(info);
	  }else {
	  	res.status(response.statusCode).json(body);
	  }
	}
	request(options, callback); 	
}