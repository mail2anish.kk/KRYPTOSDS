var bcrypt = require('bcryptjs');

var dbUtil = require("../../config/dbUtil");
var ObjectId = require('mongodb').ObjectID;
var extrequest = require('request');

var smsurl = "http://203.129.203.243/blank/sms/user/urlsms.php?username=dealmonk&pass=112233&senderid=DLMONK&response=Y";

/**
 * 
 *
 * @param  {[type]}   req  [description]
 * @param  {[type]}   res  [description]
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
exports.createuser = function(req, res, next) {
	
	if (req.body && req.body.tenant  && req.body.otp && req.body.rollno && req.body.pwd && req.body.cpwd) {
		var rollno = req.body.rollno;
		var tenant = req.body.tenant;
		var otp = req.body.otp;
		var pwd= req.body.pwd;
		var cpwd = req.body.cpwd;
		if(pwd != cpwd) {
			res.json({"error" : "Password and Confirm password do not match."});
			return;
		}
		dbUtil.getConnection(function(db){
			var tableName = "T_" + req.body.tenant + "_OTP";
			db.collection(tableName).find({"tenant" : tenant, "rollno": rollno, "otp" : otp}).toArray(function(err, result){
				if (!result || result.length === 0) {
					res.json({"error" : "Invalid OTP"});
				}else {
					var record = result[0];
					var tenantUserTable = "T_" + tenant + "_USERS";
					db.collection("T_" + tenant + "_USERS").find({"tenant" : tenant, "rollno": rollno}).toArray(function(err, result2){
						if (!result2 || result2.length === 0) {
							var userrecord = {"rollno" : rollno, "tenant" : tenant, "firstname" : record.firstname,
									"lastname" : record.lastname, "email" : record.email, "phone" : record.phone};
							bcrypt.hash(pwd, 8, function(err, hash) {
								userrecord.pwd = hash;
								db.collection(tenantUserTable).insertOne(userrecord, function(err, result3) {
									res.json({"success" : "User created successfully"});
								});
							});							
						}else {
							res.json({"error" : "User already exists."});
						}
					});
				}
			});
		});
	}else {
		res.status(401).json({"Error":"Body should contain 'otp',  'rollno', 'tenant', 'pwd', 'cpwd'"});
	}
		
}


exports.authenticate = function(req, res, next) {
	
	if (req.body && req.body.tenant   && req.body.username && req.body.password) {
		var tenant = req.body.tenant;
		var username = req.body.username;
		var password = req.body.password;
		var tenantUserTable = "T_" + tenant + "_USERS";
		dbUtil.getConnection(function(db){
			db.collection("T_" + tenant + "_USERS").find({"tenant" : tenant, "email": username}).toArray(function(err, result2){
				if (!result2 || result2.length === 0) {
					res.status(401).json({"error" : "Invalid user name or password."});
										
				}else {
					var userrecord = result2[0];
					bcrypt.compare(password, userrecord.pwd, function(err, cres) {
	    				console.log("Compare " + cres);
	    				if(cres) {
	    					var loggeduser = userrecord;
	    					var token = userrecord.pwd;
	    					loggeduser.usertoken =  token;
	    					delete loggeduser.pwd;
	    					res.json({"success" : "Login Successful", "userinfo" : loggeduser});
	    				}else {
	    					res.status(401).json({"error" : "Invalid user name or password."});
	    				}
					});	
				}
			});
		});
	}else {
		res.status(401).json({"Error":"Body should contain 'username',   'tenant', 'password'"});
	}
		
}


exports.updateuser = function(req, res, next) {
	
	if (req.body && req.body.tenant  && req.body.rollno && req.body._id && req.body.usertoken) {
		var rollno = req.body.rollno;
		var tenant = req.body.tenant;
		if (!(req.body._id.match(/^[0-9a-fA-F]{24}$/))) {
			res.status(401).json({"Error":"Invalid Object Id "+id});
			return;
		}
		var id = new ObjectId(req.body._id);
		delete req.body._id;
		dbUtil.getConnection(function(db){
			var tableName = "T_" + req.body.tenant + "_USERS";
			db.collection(tableName).find({"_id": id, "pwd" : req.body.usertoken }).toArray(function(err, result){
	 			if (!result || result.length === 0) {
	 				res.status(401).json({"Error":"No matching record found for update"});
	 			} else {
	 				delete req.body.usertoken;
	 				delete req.body.tenant;
	 				delete req.body.rollno;
					db.collection(tableName).updateOne({"_id": id}, {$set : req.body} , function(err, result){
						console.log("Updating the Client Data record with id "+id);
						//res.json(req.body);
						db.collection(tableName).find({"_id": id}).toArray(function(err, result){
							var userinfo = result[0];
							userinfo.usertoken = userinfo.pwd;
							delete userinfo.pwd;
							res.json({"success" : "Successfully updated the data", "userinfo" : userinfo});
						});
					});
				}
			});
		});
	}else {
		res.status(401).json({"Error":"Body should contain   'rollno', 'tenant', '_id', 'usertoken'"});
	}
		
}

exports.getuser = function(req, res, next) {
	
	if (req.body && req.body.tenant  && req.body._id && req.body.usertoken) {
		var tenant = req.body.tenant;
		if (!(req.body._id.match(/^[0-9a-fA-F]{24}$/))) {
			res.status(401).json({"Error":"Invalid Object Id "+id});
			return;
		}
		var id = new ObjectId(req.body._id);
		delete req.body._id;
		dbUtil.getConnection(function(db){
			var tableName = "T_" + req.body.tenant + "_USERS";
			db.collection(tableName).find({"_id": id, "pwd" : req.body.usertoken }).toArray(function(err, result){
	 			if (!result || result.length === 0) {
	 				res.status(401).json({"Error":"No matching record found"});
	 			} else {
	 				var userInfo = result[0];
	 				var token = userInfo.pwd;
	 				delete userInfo.pwd;
	 				userInfo.usertoken = token;
	 				res.json(userInfo);
				}
			});
		});
	}else {
		res.status(401).json({"Error":"Body should contain  'tenant', '_id', 'usertoken'"});
	}
		
}


exports.forgotpassword = function(req, res, next) {
	
	if (req.body && req.body.tenant   && req.body.username ) {
		var tenant = req.body.tenant;
		var username = req.body.username;
		var tenantUserTable = "T_" + tenant + "_USERS";
		dbUtil.getConnection(function(db){
			db.collection("T_" + tenant + "_USERS").find({"tenant" : tenant, "email": username}).toArray(function(err, result2){
				if (!result2 || result2.length === 0) {
					res.status(401).json({"error" : "User not found."});
										
				}else {
					var userrecord = result2[0];

					var tableName = "T_" + req.body.tenant + "_FP_OTP";
					db.collection(tableName).find({"tenant" : tenant, "email": username}).toArray(function(err, result){
			 			if (!result || result.length === 0) {
							var OTP = Math.floor(Math.random()*90000) + 10000;
							var otprecord = {"tenant" : tenant, "email": username, "otp" : OTP};
			 				db.collection(tableName).insertOne(otprecord, function(err, result) {
			    				//console.log("Inserted a document into the " + tableName + " collection.");
			    				var url = smsurl + "&dest_mobileno=" + userrecord.phone + "&message=" + OTP + " is your One-Time Password (OTP) for " + tenant;
			    				//console.log('SMS request : ' + url);
			    				extrequest(url, function(err, resp, body) {
			    					//console.log("SMS Resp : " + body);

			    					otprecord.smsresp = body;
			    					var msg = OTP + " is your One-Time Password (OTP) for " + tenant;
			    					var emaildata = {"from": "donotreply@kryptosmobile.com", "to":userrecord.email, "subject" : msg, "body" : msg};
			    					var emailOptions = {
			    						url : "https://kryptos.kryptosmobile.com/gateway/CEAI/kryptosemailsender",
			    						headers : {
			    							'licenseKey' : "UjpIaWVDVzpTOjE0NDk0ODk2MzUxMzE6VTpha0BjYW1wdXNlYWkub3JnOlQ6NDpQOjI4Ng=="
			    						},
			    						body : JSON.stringify(emaildata)
			    					}

			    					extrequest.post(emailOptions, function(err, eresp, ebody) {
			    						//console.log("Email resp : " + ebody);
			    						//console.log("Email err : " + err);
			    						//console.log("Email eresp : " + eresp);			    						
										otprecord.emailresp = ebody;
										res.json(otprecord);
			    					});			    					
			    				});
			  				});
			 			} else {
			 				//console.log("result.length " + result.length);
			 				var record = result[0];
			 				var id = new ObjectId(record._id);
			 				var OTP = Math.floor(Math.random()*90000) + 10000;
			 				record.otp = OTP;
			 				//console.log(record);
			 				delete record._id;
			 				db.collection(tableName).replaceOne({"_id": id}, record, function(err, result2){
								//console.log("Updating the record with id "+id);
								var url = smsurl + "&dest_mobileno=" + userrecord.phone + "&message=" + OTP + " is your One-Time Password (OTP) for " + tenant;
								//console.log('SMS request : ' + url);

			    				extrequest(url, function(err, resp, body) {
			    					//console.log("SMS Resp : " + body);
			    					record.smsresp = body;
			    					//res.json(record);
			    					var msg = OTP + " is your One-Time Password (OTP) for " + tenant;
			    					var emaildata = {"from": "donotreply@kryptosmobile.com", "to":userrecord.email, "subject" : msg, "body" : msg};
			    					var emailOptions = {
			    						url : "https://kryptos.kryptosmobile.com/gateway/CEAI/kryptosemailsender",
			    						headers : {
			    							'licenseKey' : "UjpIaWVDVzpTOjE0NDk0ODk2MzUxMzE6VTpha0BjYW1wdXNlYWkub3JnOlQ6NDpQOjI4Ng=="
			    						},
			    						body : JSON.stringify(emaildata)
			    					}

			    					extrequest.post(emailOptions, function(err, eresp, ebody) {
			    						//console.log("Email resp : " + ebody);
			    						//console.log("Email err : " + err);
			    						//console.log("Email eresp : " + eresp);
										record.emailresp = ebody;
										res.json(record);
			    					});	
			    				});
							});							
						}
					});

				}
			});
		});
	}else {
		res.status(401).json({"Error":"Body should contain 'username',   'tenant'"});
	}
		
}


exports.resetpassword = function(req, res, next) {
	
	if (req.body && req.body.tenant  && req.body.otp && req.body.username && req.body.pwd && req.body.cpwd) {
		var username = req.body.username;
		var tenant = req.body.tenant;
		var otp = req.body.otp;
		var pwd= req.body.pwd;
		var cpwd = req.body.cpwd;
		if(pwd != cpwd) {
			res.json({"error" : "Password and Confirm password do not match."});
			return;
		}
		dbUtil.getConnection(function(db){
			var tableName = "T_" + req.body.tenant + "_FP_OTP";
			db.collection(tableName).find({"tenant" : tenant, "email": username, "otp" : otp}).toArray(function(err, result){
				if (!result || result.length === 0) {
					res.json({"error" : "Invalid OTP"});
				}else {
					var record = result[0];
					var tenantUserTable = "T_" + tenant + "_USERS";
					db.collection("T_" + tenant + "_USERS").find({"tenant" : tenant, "email": username}).toArray(function(err, result2){
						if (!result2 || result2.length === 0) {
							res.json({"error" : "User not found."});												
						}else {
							var userrecord = result2[0];
							bcrypt.hash(pwd, 8, function(err, hash) {
								console.log(userrecord);
								var updata = {"pwd" : hash};
								db.collection(tenantUserTable).updateOne({"_id" : userrecord._id}, {$set : updata } , function(err, result){
									res.json({"success" : "Successfully updated the password"});
								});
							});		
						}
					});
				}
			});
		});
	}else {
		res.status(401).json({"Error":"Body should contain 'otp',  'username', 'tenant', 'pwd', 'cpwd'"});
	}
		
}

exports.changepassword = function(req, res, next) { 
	if (req.body && req.body.tenant  && req.body.oldpwd && req.body.username && req.body.pwd && req.body.cpwd) {
		var username = req.body.username;
		var tenant = req.body.tenant;
		var oldpwd = req.body.oldpwd;
		var pwd= req.body.pwd;
		var cpwd = req.body.cpwd;
		var tenantUserTable = "T_" + tenant + "_USERS";
		dbUtil.getConnection(function(db){
			db.collection("T_" + tenant + "_USERS").find({"tenant" : tenant, "email": username}).toArray(function(err, result2){
				if (!result2 || result2.length === 0) {
					res.status(401).json({"error" : "User not found."});
										
				}else {
					var userrecord = result2[0];
					bcrypt.compare(oldpwd, userrecord.pwd, function(err, cres) {
	    				console.log("Compare " + cres);
	    				if(cres) {
	    					if(pwd != cpwd) {
								res.json({"error" : "Password and Confirm password do not match."});
								return;
							}
							bcrypt.hash(pwd, 8, function(err, hash) {
								//console.log(userrecord);
								var updata = {"pwd" : hash};
								db.collection(tenantUserTable).updateOne({"_id" : userrecord._id}, {$set : updata } , function(err, result){
									res.json({"success" : "Successfully updated the password"});
								});
							});		
	    				}else {
	    					res.status(401).json({"error" : "Invalid old password."});
	    				}
					});	
				}
			});
		});
	}else {
		res.status(401).json({"Error":"Body should contain 'oldpwd',  'username', 'tenant', 'pwd', 'cpwd'"});
	}
}