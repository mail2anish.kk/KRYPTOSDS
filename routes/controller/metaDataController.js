var dbUtil = require("../../config/dbUtil");
var ObjectId = require('mongodb').ObjectID;

module.exports.createMetaData = function(req, res, next) {
	dbUtil.getConnection(function(db){
		if (req.body._id) {
			var id = new ObjectId(req.body._id);
			delete req.body._id;
			db.collection('MetaData').replaceOne({"_id": id}, req.body, function(err, result){
				console.log("Updating the record with id "+id);
				res.json(req.body);
			});
		} else {
			console.log(req.body);
			db.collection('MetaData').insertOne(req.body, function(err, result) {
		    	console.log("Inserted a document into the MetaData collection.");
			 	res.json(req.body);
		  	});
		}
	});
}
