var dbUtil = require("../../config/dbUtil");
var ObjectId = require('mongodb').ObjectID;
var q = require('q');
var _ = require('lodash');

/**
 * Constructs a table name dynamically based on the params passed.
 *
 * @param  {[type]} clientId   [description]
 * @param  {[type]} appId      [description]
 * @param  {[type]} featureId  [description]
 * @param  {[type]} subFeature [description]
 * @return {[type]}            [description]
 */
var getTableName = function(clientId, appId, featureId, subFeature) {
	return "T_" + subFeature+"_"+clientId+"_"+appId+"_"+featureId;
}

/**
 * Create tenants Data
 *
 * @param  {[type]}   req  [description]
 * @param  {[type]}   res  [description]
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
exports.createTenantData = function(req, res, next) {
	console.log("create TenantData called");
	dbUtil.getConnection(function(db){
		var clientId = req.params.clientId;
		var appId = req.params.appId;
		var featureId = req.params.featureId;
		var subFeature = req.query.subFeature;
		if (clientId && appId && featureId && subFeature) {
			var tableName = getTableName(clientId, appId, featureId, subFeature);
			if (req.body._id) {
				if (!(req.body._id.match(/^[0-9a-fA-F]{24}$/))) {
				  res.status(401).json({"Error":"Invalid Object Id "+id});
				}
				var id = new ObjectId(req.body._id);
				delete req.body._id;
				// check whether the data with that id exists else throw error
				db.collection(tableName).find({"_id": id}).toArray(function(err, result){
		 			if (!result || result.length === 0) {
		 				res.status(401).json({"Error":"No element with supplied is "+id+" exists"});
		 			} else {
						db.collection(tableName).replaceOne({"_id": id}, req.body, function(err, result){
							console.log("Updating the Client Data record with id "+id);
							res.json(req.body);
						});
					}
				});
			} else {
				db.collection(tableName).insertOne(req.body, function(err, result) {
			    	console.log("Inserted a document into the Client Data collection.");
				 	res.json(req.body);
			  	});
			}
		}
	});
}


/**
 * Delete tenants Data
 *
 * @param  {[type]}   req  [description]
 * @param  {[type]}   res  [description]
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
exports.deleteTenantData = function(req, res, next) {
	console.log("delete TenantData called");
	dbUtil.getConnection(function(db){
		var clientId = req.params.clientId;
		var appId = req.params.appId;
		var featureId = req.params.featureId;
		var dataid = req.params.dataid;
		var subFeature = req.query.subFeature;
		if (clientId && appId && featureId && subFeature) {
			var tableName = getTableName(clientId, appId, featureId, subFeature);
			if (dataid) {
				if (!(dataid.match(/^[0-9a-fA-F]{24}$/))) {
				  res.status(401).json({"Error":"Invalid Object Id "+id});
				}
				var id = new ObjectId(dataid);
				// check whether the data with that id exists else throw error
				db.collection(tableName).find({"_id": id}).toArray(function(err, result){
		 			if (!result || result.length === 0) {
		 				res.status(401).json({"Error":"No element with supplied is "+id+" exists"});
		 			} else {
						db.collection(tableName).deleteOne({"_id": id},  function(err, result){
							console.log("Deleting the Client Data record with id "+id);
							res.json(req.body);
						});
					}
				});
			}else {
				res.status(401).json({"Error":"ID not found in the request"});
			}
		}else {
				res.status(401).json({"Error":"Invalid request."});
		}
	});
}


/**
 * Query Tenant Specific data.
 *
 * @param  {[type]}   req  [description]
 * @param  {[type]}   res  [description]
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
exports.queryTenantData = function(req, res, next) {
	dbUtil.getConnection(function(db){
		var clientId = req.params.clientId;
		var appId = req.params.appId;
		var featureId = req.params.featureId;
		var subFeature = req.query.subFeature;
		if (clientId && appId && featureId && subFeature) {
			// Check if id is there
			var priKey = req.body._id;
			if (priKey) {
				req.body._id = new ObjectId(priKey)
			}
			var tableName = getTableName(clientId, appId, featureId, subFeature);
			db.collection(tableName).find(req.body).toArray(function(err, result){
				res.json(result);
			});
		}
	});
}

/**
 * Query Tenant data using wildcard.
 *
 * @param  {[type]}   req  [description]
 * @param  {[type]}   res  [description]
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
exports.queryTenantDataUsingKeyword = function(req, res, next) {
	dbUtil.getConnection(function(db){
		var clientId = req.params.clientId;
		var appId = req.params.appId;
		var featureId = req.params.featureId;
		var subFeature = req.query.subFeature;
		if (clientId && appId && featureId && subFeature) {

			getTenantMetaData(clientId, appId, featureId, subFeature)
				.then(function(response){
				var myjson = req.body
				var temp = [];
				//Create regex object for each value
				for(var i=0; i<response.metadata.length; i++) {
					var fieldid = response.metadata[i].fieldid;
					var data = {};
					if(myjson[fieldid]) {
						data[fieldid] = new RegExp(myjson[fieldid],"i");
						temp.push(data);
					}
				}
				var tableName = getTableName(clientId, appId, featureId, subFeature);
				db.collection(tableName).find({ "$or": temp}).toArray(function(err, result){
					res.json(result);
				});
			});
		}
	});
}

/**
 * Query tenants Data by ID
 *
 * @param  {[type]}   req  [description]
 * @param  {[type]}   res  [description]
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
exports.queryTenantDataById = function(req, res, next) {
	console.log("Query TenantData by Id called");
	dbUtil.getConnection(function(db){
		var clientId = req.params.clientId;
		var appId = req.params.appId;
		var featureId = req.params.featureId;
		var dataid = req.body._id;
		var subFeature = req.query.subFeature;
		if (clientId && appId && featureId && subFeature) {
			var tableName = getTableName(clientId, appId, featureId, subFeature);
			if (dataid) {
				if (!(dataid.match(/^[0-9a-fA-F]{24}$/))) {
				  res.status(401).json({"Error":"Invalid Object Id "+id});
				}
				var id = new ObjectId(dataid);
				// check whether the data with that id exists else throw error
				db.collection(tableName).find({"_id": id}).toArray(function(err, result){
		 			if (!result || result.length === 0) {
		 				res.status(401).json({"Error":"No element with supplied is "+id+" exists"});
		 			} else {
						db.collection(tableName).findOne({"_id": id},  function(err, result){
							res.json(result);
						});
					}
				});
			}else {
				res.status(401).json({"Error":"ID not found in the request"});
			}
		}else {
				res.status(401).json({"Error":"Invalid request."});
		}
	});
}

/**
 * Query tenants metadata information.
 *
 * @param  {[type]}   req  [description]
 * @param  {[type]}   res  [description]
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
exports.queryTenantMetaData = function(req, res, next) {
	var clientId = req.params.clientId;
	var appId = req.params.appId;
	var featureId = req.params.featureId;
	var subFeature = req.query.subFeature;
	if (clientId && appId && featureId && subFeature) {
			console.log("Inside if of query tenant metadata : ");
			getTenantMetaData(clientId, appId, featureId, subFeature)
			.then(function(response){
				console.log("Response : " + response);
				res.json(response);
			});
	} else {
		console.log("Going to else on query Tenant metadata");
		res.json({"Error":"All parameters for querying metadata was not supplied","Params Supplied":{
					"Operation":"QueryTenantData",
					"clientId":req.params.clientId,
					"appId":req.params.appId,
					"featureId":req.params.featureId,
					"subFeature":req.query.subFeature
				}});

	}
}

/**
 * Checks whether the tenant data can be created. Will validated the incoming colum against
 * the metadata information stored against the tenant.
 *
 * @param  {[type]}   req  [description]
 * @param  {[type]}   res  [description]
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
exports.canCreateTenantData = function(req, res, next) {
	console.log("Inside can create tenant data");
	var clientId = req.params.clientId;
	var appId = req.params.appId;
	var featureId = req.params.featureId;
	var subFeature = req.query.subFeature;
	if (clientId && appId && featureId && subFeature) {
			console.log("Inside if");
			getTenantMetaData(clientId, appId, featureId, subFeature)
			.then(function(response){
				// Check the keys in database against the key sent in the data to insert.
				var masterKeys = response.metaData.map(function(obj){
					return obj.key;
				}).sort();
				var inputKeys = Object.keys(req.body).sort();
				// Request could be an update request where there could be an additonal field named "_id" present
				// Such request also should be allowed to go through to the next middleware function.
				var masterKeysWithId = _.clone(masterKeys);
				masterKeysWithId.push("_id");
				masterKeysWithId.sort();
				if ((JSON.stringify(masterKeys) === JSON.stringify(inputKeys)) || (JSON.stringify(masterKeysWithId) === JSON.stringify(inputKeys))) {
					next();
				} else {
					res.status("401").json({"Error":"Column mismatch"});
				}
			});
	} else {
		res.json({"Error":"All parameters for creating metadata was not supplied","Params Supplied":{
					"Operation":"QueryTenantData",
					"clientId":req.params.clientId,
					"appId":req.params.appId,
					"featureId":req.params.featureId,
					"subFeature":req.query.subFeature
				}});
	}
}


var getTenantMetaData = function(clientId, appId, featureId, subFeature) {
	var deferred = q.defer();
	var aid = isNaN(appId) ? appId : parseInt(appId);
	var fid = isNaN(featureId) ? featureId : parseInt(featureId);
	dbUtil.getConn()
	.then(function(db){
	 	db.collection('MetaData').find({"clientId": clientId, "appId": aid, "featureId": fid, "subFeature": subFeature}).toArray(function(err, result){
	 			console.log("inside callback of find " + result);
	 			if (result && result.length >0) {
	 				result = result[0];
	 			}
				deferred.resolve(result);
			});
	});
 	return deferred.promise;
}
