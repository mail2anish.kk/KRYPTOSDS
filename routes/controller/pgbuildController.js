var phonegapbuild = require('phonegap-build');

/**
 * Uploads a base 64 encoded image to the filesystem and returns the url.
 *
 * @param  {[type]}   req  [description]
 * @param  {[type]}   res  [description]
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
exports.pgblogin = function(req, res, next) {
	
	var options = { username: req.body.username, password: req.body.password };

	phonegapbuild.login(options, function(e, api) {
    	// now logged in
    	console.log("Logged In");
    	console.log(api);
    	console.log(e);
		//res.json({"result":"Logged In"});    	
	});
	res.json({"result":"Success"});
}
